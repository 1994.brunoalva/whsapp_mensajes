<?php



Route::post("/login","UsuarioController@login");
Route::post("/cliente/lista","ClienteController@lista");
Route::post("/cliente/registrar","ClienteController@registrar");
Route::post("/cliente/lista/consul","ClienteController@lista_consulta");
Route::post("/cliente/enviar/mensaje","ConsultaController@enviar_mensaje");
Route::post("/ajs/save/session","ConsultaController@save_session_user");
Route::post("/ajs/get/session","ConsultaController@get_session_user");


Route::post("/upd/file/wsv","UploadFileController@guardar_archivo");