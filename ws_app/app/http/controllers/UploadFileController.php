<?php


class UploadFileController extends Controller
{

    public function guardar_archivo(){
        $filename = $_FILES['file']['name'];

        $path_parts = pathinfo($filename, PATHINFO_EXTENSION);
        $newName =Tools::getToken(80);
        /* Location */
        $loc_ruta="./files";
        if (!file_exists($loc_ruta)) {
            mkdir($loc_ruta, 0777, true);
        }
        $location = $loc_ruta."/" . $newName .'.'. $path_parts;
        $uploadOk = 1;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);

        $arr = array( 'res' => false);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
            $arr['res']=true;
            $arr['dstos']= $newName.".".$path_parts;
            echo json_encode($arr);
        } else {
            echo json_encode($arr);
        }
    }

}