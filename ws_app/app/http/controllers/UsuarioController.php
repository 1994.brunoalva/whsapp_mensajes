<?php


class UsuarioController extends Controller
{
    private $usuario;

    /**
     * UsuarioController constructor.
     */
    public function __construct()
    {
        $this->usuario=new UsuarioDao();
    }


    public function login($req){
        $this->usuario->setUsuario($_POST['user']);
        $this->usuario->setClave($_POST['password']);
        return $this->usuario->login();
    }
    public function logout(){
        session_destroy();
        header("Location: ".URL::to("login"));
        exit;
    }
}