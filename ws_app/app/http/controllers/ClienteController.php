<?php


class ClienteController extends Controller
{
    private $cliente;

    public function __construct(){
        $this->cliente=new ClienteDao();
    }

    public function registrar(){
        $this->cliente->setNombres($_POST['nombre']);
        $this->cliente->setCelular($_POST['numero']);
        $this->cliente->setDocPais('51');
        return $this->cliente->insert();
    }
    public function lista(){
        return json_encode($this->cliente->listar());
    }
    public function lista_consulta(){
        return $this->cliente->listar_consulta($_POST["lista"]);
    }

}