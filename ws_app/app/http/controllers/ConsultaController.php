<?php


class ConsultaController extends Controller
{
    private $consulta;

    public function __construct()
    {
        $this->consulta=new ConsultaDatos();
    }
    public function get_session_user(){
        return $this->consulta->get_sessionUsuario();
    }
    public function save_session_user(){
        return $this->consulta->guardar_sesionCliente($_POST["session"]);
    }
    public function enviar_mensaje(){
        $this->consulta->enviar_mensajer(json_decode($_POST['lista'],true),$_POST['mesnaje']);
    }

}