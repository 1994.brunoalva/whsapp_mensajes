<?php


class ViewController extends Controller
{
    public function index(){
        return $this->view("index");
    }

    public function login(){
        return $this->view("page-login");
    }
    public function envio_mensajes(){
        $cliente=new ClienteDao();

        return $this->view("fragment_views/mensajes",["clientes"=>$cliente->listar()]);
    }

}