<?php


class ConsultaDatos
{
    private $conexion;
    private $respuesta;

    function __construct()
    {
        $this->conexion=(new Conexion())->getConexion();
        $this->respuesta=["res"=>false];
    }
    public function get_sessionUsuario(){
        $sql="select * from usuarios where usuario_id='{$_SESSION['usuario_scp']}'";

        if ($row = $this->conexion->query($sql)->fetch_assoc()){
            if (!is_null($row['session_ws'])){
                if (strlen($row['session_ws'])>0){
                    $this->respuesta['res']=true;
                    $this->respuesta['auth']=$row['session_ws'];
                }
            }
        }
        return json_encode($this->respuesta);
    }
    public function guardar_sesionCliente($session){
        $sql="update usuarios set session_ws=? where usuario_id = '{$_SESSION['usuario_scp']}'";
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("s", $session);
        if ($stmt->execute()){
            $this->respuesta['res']=true;
        }
        return json_encode($this->respuesta);
    }

    public function enviar_mensajer($lista,$mensaje){
        $temp_arr=[];
        foreach ($lista as $ro){
            $temp_arr[]="cliente_id='".Tools::decrypt($ro)."'";
        }
        $sql=" select * from clientes where ".implode(" OR ",$temp_arr);
        $resul = $this->conexion->query($sql);
        $clientes_lista_numero=[];
        foreach ($resul as $item) {
            $clientes_lista_numero[]=$item["doc_pais"].$item['celular'];
        }

        echo json_encode([
            "mensaje"=>$mensaje,
            "archivo"=>"",
            "lista"=>$clientes_lista_numero
        ]);
    }
}