<?php


class UsuarioDao extends Usuario
{
    private $conexion;
    private $respuesta;

    function __construct()
    {
        $this->conexion=(new Conexion())->getConexion();
        $this->respuesta=["res"=>false];
    }

    public function login(){
        $sql="SELECT * FROM usuarios WHERE email ='{$this->getUsuario()}' OR usuario = '{$this->getUsuario()}' ";
        $resp = $this->conexion->query($sql);
        if ($row = $resp->fetch_assoc()){
            if ($row['clave']==sha1($this->getClave())){
               /* $sql ="SELECT * FROM roles WHERE rol_id='{$row['id_rol']}'";
                $resp_rol = $this->conexion->query($sql)->fetch_assoc();*/

                $_SESSION["usuario_scp"]=$row['usuario_id'];
                $_SESSION["scp-nombre"]=$row['nombre'].' '.$row['apellido'];
                $_SESSION["scp-email"]=$row['email'];

                $this->respuesta["res"]=true;
            }else{
                $this->respuesta["msg"]="Contraseña incorrecta";
            }
        }else{
            $this->respuesta["msg"]="Usuario no encontrado";
        }

        return json_encode($this->respuesta);
    }

}