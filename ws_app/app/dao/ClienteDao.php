<?php


class ClienteDao extends Cliente
{

    private $conexion;
    private $respuesta;

    public function __construct(){
        $this->conexion=(new  Conexion())->getConexion();
        $this->respuesta=["res"=>false];
    }
    public function insert(){
        $sql ="insert into clientes set celular='{$this->getCelular()}', nombres='{$this->getNombres()}', 
             estado='1', id_usuario='{$_SESSION['usuario_scp']}', doc_pais='{$this->getDocPais()}'";
        if ($this->conexion->query($sql)){
            $this->respuesta['res']=true;
        }
        return json_encode($this->respuesta);
    }
    public function listar(){
        $sql ="select * from clientes where id_usuario='{$_SESSION['usuario_scp']}'";
        $this->respuesta=[];
        $resul = $this->conexion->query($sql);
        foreach ($resul as $row){
            $row['cliente_id'] = Tools::encrypt($row['cliente_id']);
            $this->respuesta[]=$row;
        }
        return $this->respuesta;
    }
    public function listar_consulta($lista){
        $temp_list = json_decode($lista);
        $lista_final =[];
        foreach ($temp_list as $cli){
            $lista_final[]=" cliente_id = ".Tools::decrypt($cli);
        }
        $sql ="select * from clientes where ".implode(" OR ",$lista_final);
        $this->respuesta=[];
        $resul = $this->conexion->query($sql);
        foreach ($resul as $row){
            $this->respuesta[]=$row['doc_pais'].$row["celular"];
        }
        return json_encode($this->respuesta);
    }


}