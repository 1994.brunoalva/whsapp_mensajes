var temp ;
//$("#loader-menor").show()
$(document).ready(function () {

    $("#page-content-").on("click", ".dom-link", function(evt){
        const url_ter = $( evt.currentTarget).attr('href');
        const titulo  = $( evt.currentTarget).attr('title');
        console.log(url_ter);
        renombrarURL(_URL+url_ter,titulo);
        _ajaxDOM(url_ter,'page-content-')
        evt.preventDefault();
    });

    _ajaxDOM(getPathURL(),'page-content-')
    $(".menu-link").click(function (evt) {
        //$("#loader-menor").show()
        const url_ter = $( evt.currentTarget).attr('href');
        const titulo  = $( evt.currentTarget).attr('title');
        console.log(url_ter);
        renombrarURL(_URL+url_ter,titulo);
        if (divice=='tablet'||divice=='mobile'){
            $(".wrapper").removeClass("toggled")
        }
        _ajaxDOM(url_ter,'page-content-')
        evt.preventDefault();
    })
    $(".menu-link-no").click(function (evt) {
        alertAdvertencia("Modulo no activdado","Para usar este modulo, comuniquese con el administrador")
        evt.preventDefault();
    })
})

function abreviaturaMes(mes){
    const meses = ["EN","FEBR","MZO","ABR","MY","JUN","JUL","AGTO","SPT","OCT","NOV","DIC"]

    return meses[$mes];

}
function  abreviaturaMesIngles(mes){
    const meses = ["JAN","FEB","MAR","APR","MAY","JUNE","JULY","AUG","SEPT","OCT","NOV","DEC"];

    return meses[mes];

}
function getMesAbreLinst(idioma) {
    if (idioma=='es'){
        return ["EN","FEBR","MZO","ABR","MY","JUN","JUL","AGTO","SPT","OCT","NOV","DIC"];
    }else {
        return   ["JAN","FEB","MAR","APR","MAY","JUNE","JULY","AUG","SEPT","OCT","NOV","DEC"];
    }
}
function renombrarURL(url,titulo='',data=null) {
    window.history.pushState(data, titulo, url);
}
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function getPathURL() {
    return location.href.slice(_URL.length);
}
function _ajaxDOM(url,contenedor_id) {
    $.ajax({
        type: 'POST',
        url: _URL+url,
        success: function (resp) {
            $("#loader-menor").hide()
            $("#"+contenedor_id).html(resp);
        }
    });

}

function alertError(title,msg){

    return Swal.fire({icon: 'error',
        title: title,
        text: msg,})
}
function alertExito(title,msg){
    return Swal.fire({icon: 'success',
        title: title,
        text: msg,})
}
function alertAdvertencia(title,msg){
    return Swal.fire({icon: 'warning',
        title: title,
        text: msg,})
}
function alertInfo(title,msg){
    return Swal.fire({icon: 'info',
        title: title,
        text: msg,})
}

function _ajax(url,method,data={},func) {
    $.ajax({
        type: method,
        url: _URL+url,
        data: data,
        success: function (resp) {
            $("#loader-menor").hide()
            if (isJson(resp)){
                func(JSON.parse(resp));
            }else{
                console.log(resp)
                alertError('ERR','Error en el servidor')
            }

        }
    });

}

function reloadPagDon() {

}