<div id="sidebar-nav" class="sidebar">
    <nav>
        <ul class="nav" id="sidebar-nav-menu">
            <li class="menu-group">Main</li>
            <li><a href="/" class=""><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a></li>
            <li class="panel">
                <a href="#dashboards" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded="true" class="active">
                    <i class="ti-dashboard"></i> <span class="title">Dashboards</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="dashboards" class="collapse show ">
                    <ul class="submenu">
                        <li><a href="index.html" class="active">Dashboard v1</a></li>
                        <li><a href="dashboard2.html" class="">Dashboard v2</a></li>
                        <li><a href="dashboard3.html" class="">Dashboard v3</a></li>
                        <li><a href="dashboard4.html" class="">Dashboard v4</a></li>
                    </ul>
                </div>
            </li>
            <li class="panel">
                <a href="#subLayouts" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded="" class=""><i class="ti-layout"></i> <span class="title">Layouts</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subLayouts"  class="collapse  ">
                    <ul class="submenu">
                        <li><a href="layout-topnav.html" class="">Top Navigation</a></li>
                        <li><a href="layout-minified.html" class="">Minified</a></li>
                        <li><a href="layout-fullwidth.html" class="">Fullwidth</a></li>
                        <li><a href="layout-default.html" class="">Default</a></li>
                        <li><a href="layout-grid.html">Grid</a></li>
                    </ul>
                </div>
            </li>

            <li class="panel">
                <a href="#forms" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded="" class=""><i class="ti-receipt"></i> <span class="title">Forms</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="forms" class="collapse  ">
                    <ul class="submenu">
                        <li><a href="forms-inputs.html" class="">Inputs</a></li>
                        <li><a href="forms-multiselect.html" class="">Multiselect</a></li>
                        <li><a href="forms-input-pickers.html" class="">Input Pickers</a></li>
                        <li><a href="forms-input-sliders.html" class="">Input Sliders</a></li>
                        <li><a href="forms-select2.html" class="">Select2</a></li>
                        <li><a href="forms-inplace-editing.html" class="">In-place Editing</a></li>
                        <li><a href="forms-dragdropupload.html" class="">Drag and Drop Upload</a></li>
                        <li><a href="forms-layouts.html" class="">Form Layouts</a></li>
                        <li><a href="forms-validation.html" class="">Form Validation</a></li>
                        <li><a href="forms-texteditor.html" class="">Text Editor</a></li>
                    </ul>
                </div>
            </li>
            <li class="panel">
                <a href="#appViews" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded="" class=""><i class="ti-layout-tab-window"></i> <span class="title">App Views</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="appViews" class="collapse  ">
                    <ul class="submenu">
                        <li><a href="appviews-project-detail.html" class="">Project Details</a></li>
                        <li><a href="appviews-projects.html" class="">Projects</a></li>
                        <li><a href="appviews-inbox.html" class="">Inbox <span class="badge badge-danger">8</span></a></li>
                        <li><a href="appviews-file-manager.html" class="">File Manager</a></li>
                    </ul>
                </div>
            </li>


        </ul>
        <button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-arrows-horizontal"></i></button>
    </nav>
</div>