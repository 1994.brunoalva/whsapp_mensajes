<!doctype html>
<html lang="en" class="fullscreen-bg">

<!-- Mirrored from demo.thedevelovers.com/dashboard/klorofilpro-v2.0/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Jul 2021 02:45:37 GMT -->
<head>
    <title>Login | Ws</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- App css -->
    <link href="<?= URL::to('public/assets/css/bootstrap-custom.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= URL::to('public/assets/css/app.min.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= URL::to('public/assets/images/favicon.png') ?>">
    <link href="<?=URL::to('public/plugin/sweetalert2.min.css')?>" rel="stylesheet" type="text/css">
    <script>
        const _URL='<?=URL::base()?>';
        const divice='<?=Tools::getInfoDeviceConect()?>';
    </script>

</head>
<body>
<!-- WRAPPER -->
<div id="wrapper" class="d-flex align-items-center justify-content-center">
    <div class="auth-box ">
        <div class="left">
            <div class="content">
                <div class="header">
                    <div class="logo text-center"><img src="<?= URL::to('public/assets/images/logo-dark.png') ?>"
                                                       alt="Klorofil Logo"></div>
                    <p class="lead">Ingresa a tu cuenta</p>
                </div>
                <form class="form-auth-small" v-on:submit.prevent="iniciar_session($event)">
                    <div class="form-group">
                        <label for="signin-email" class="control-label sr-only">Usuario o Email</label>
                        <input required type="text" class="form-control" id="signin-email" name="user" placeholder="Usuario o Email">
                    </div>
                    <div class="form-group">
                        <label for="signin-password" class="control-label sr-only">Contraseña</label>
                        <input required type="password" class="form-control" id="signin-password" name="password" placeholder="Contraseña">
                    </div>
                    <div hidden class="form-group">
                        <label class="fancy-checkbox element-left custom-bgcolor-blue">
                            <input type="checkbox">
                            <span class="text-muted">Remember me</span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">INGRESAR</button>
                    <div class="bottom">
                        <span class="helper-text"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">¿Se te olvidó tu contraseña?</a></span>
                    </div>
                </form>
            </div>
        </div>
        <div class="right">
            <div class="overlay"></div>
            <div class="content text">
                <h1 class="heading">Servicio de WhatsApp web</h1>
                <p>por MAGUS TECHNOLOGIES</p>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="<?=URL::to("public/js/main.js")?>"></script>

<script>
    var APP;

    $(document).ready(function (){
        APP=new Vue({
            el:"#wrapper",
            data:{},
            methods:{
                iniciar_session(evt){
                    _ajax("/login","POST",
                        $(evt.target).serialize(),
                        function (rep){
                            console.log(rep);
                            if (rep.res){
                                location.reload();
                            }else{
                                alertAdvertencia("Alerta",rep.msg)
                                $("body").removeClass("swal2-height-auto")
                            }
                        }
                    )
                }
            }
        });
    })

</script>

<!-- END WRAPPER -->
</body>

<!-- Mirrored from demo.thedevelovers.com/dashboard/klorofilpro-v2.0/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Jul 2021 02:45:37 GMT -->
</html>