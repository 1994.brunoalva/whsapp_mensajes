<?php include "fragment/head.php";?>
</head>
<body>
<div style="display: none" id="loader-menor">
    <div class="loadingio-spinner-ripple-sxy4azz6eae">
        <div class="ldio-a4xc0nkbxqn">
            <div></div><div></div>
        </div>
    </div>
</div>
<!-- WRAPPER -->
<div id="wrapper">

    <!-- NAVBAR -->
    <?php include "fragment/navbar.php";?>
    <!-- END NAVBAR -->

    <!-- LEFT SIDEBAR -->
    <?php include "fragment/sidebar.php";?>
    <!-- END LEFT SIDEBAR -->

    <!-- MAIN -->
    <div class="main">

        <!-- MAIN CONTENT -->
        <div id="page-content-"  class="main-content">

        </div>
        <!-- END MAIN CONTENT -->

        <!-- RIGHT SIDEBAR -->

        <!-- END RIGHT SIDEBAR -->

    </div>
    <!-- END MAIN -->

    <div class="clearfix"></div>

    <!-- footer -->
    <footer>
        <div class="container-fluid">
            <p class="copyright">&copy; 2020 <a href="https://www.themeineed.com/" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
        </div>
    </footer>
    <!-- end footer -->

    <!-- DEMO PANEL -->
    <!-- for demo purpose only, you should remove it on your project directory
    <script type="text/javascript">
        var toggleDemoPanel = function(e) {
            e.preventDefault();

            var panel = document.getElementById( 'demo-panel' );
            if ( panel.className ) panel.className = '';
            else panel.className = 'active';
        }
    </script>

    <div id="demo-panel">
        <a href="#" onclick="toggleDemoPanel(event);"><i class="fa fa-cog fa-spin"></i></a>
        <iframe src="demo-panel.html"></iframe>
    </div>-->
    <!-- END DEMO PANEL -->

</div>
<!-- END WRAPPER -->

<?php include "fragment/footer.php"?>
<script src="<?=URL::to('public/plugin/qrcodejs/qrcode.min.js')?>"></script>
<script type="text/javascript" src="http://192.168.0.7:3001/socket.io/socket.io.js"></script>

</body>

</html>