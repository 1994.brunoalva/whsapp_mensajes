const express = require('express');
const socketIO=require('socket.io');
var cors = require('cors')
const { Client,MessageMedia } = require('whatsapp-web.js'); 

const app=express();
app.use(express.urlencoded({ extended: true }))

app.use(cors());
app.set("port",3001);
const httpServer = require("http").createServer(app);



let client1;

const io=socketIO(httpServer, {
  cors: {
    origin: '*',
  },
  cookie: { 
    nombre: "my-cookie" , 
    httpOnly: true , 
    maxAge: 86400
   },
   allowEIO3: true  

}); 

app.post("/send",function (req,res){
	console.log(req.body);
	const { message, number } = req.body
	console.log(message, number);
	sendMessageAP(number, message)
	res.send({ status: 'Enviado!' })
})

io.on('connection',(socket)=>{
    console.log("conecto");
    socket.on('info',(data) => {
        console.log(data);
        io.sockets.emit('info',data);
    });

    socket.on('disconnect', function() {
      console.log('disconnect!');
 
   });
   socket.on("conectar-ws-serv",(data)=>{
    iniciarServisionWS(data);
   })
   socket.on("enviar-mensaje-file",(data)=>{
     const obj_data = require("./config.json");
      const mediaFile = MessageMedia.fromFilePath(obj_data.ruta_file+data.file)
      let numeros = data.numero;
      numeros.forEach(numero => {
        sendMedia(numero,mediaFile)
      });
 
      io.sockets.emit('envio-completo',{res:true});
	    
    });
   socket.on("enviar-mensaje-tex", (data)=>{
    console.log(client1.session)
     let mensaje = data.mensaje;
     let numeros = data.numero;
     //client1.sendMessage(numeros+'@c.us', "holaaaaaaaaaaaaaaaaaaaa mensaje de prueba");
     numeros.forEach(numero => {
       console.log(numero);
      sendMessageAP(numero,mensaje);
     });

     io.sockets.emit('envio-completo',{res:true});
     
   })
});

httpServer.listen(app.get("port"));



function iniciarServisionWS(data){

  if(data.res){
    client1 = new Client({
      session: JSON.parse(data.auth)
    });
  }else{
    client1 = new Client();
  }
  client1.on('ready', () => {
    io.sockets.emit("cliente-auth-ready",{});
	});
  client1.on('qr', (qr) => {
        console.log(qr)
        io.sockets.emit("genQrcliente",{qr});
	});
    client1.on('authenticated', (session) => {
	    console.log(session);
      io.sockets.emit("cliente-auth",{res:true,auth:session});
	});
  client1.on('auth_failure', () => {
		console.log('erro al auth');
    io.sockets.emit("cliente-auth",{res:false});
	})
   
  /*client1.on('message', async msg => {
		const {from,to,body}=msg;
		console.log(from+" "+to+" "+body);

    client1.sendMessage(from, "holaaaaaaaaaaaaaaaaaaaa mensaje de prueba");
	})*/
  client1.initialize();
}
const sendMedia = (to = null, file = null) => {
	client1.sendMessage( `${to}@c.us`, file);
}
const sendMessageAP = (to = null, mensaje = null) => {
  console.log(`${to}@c.us`, " "+mensaje)
	return client1.sendMessage( `${to}@c.us`, mensaje);
}