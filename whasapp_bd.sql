/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.4.14-MariaDB : Database - whatsapp_app
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `clientes` */

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `nombres` varchar(225) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `doc_pais` varchar(10) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`cliente_id`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `clientes` */

LOCK TABLES `clientes` WRITE;

insert  into `clientes`(`cliente_id`,`id_usuario`,`nombres`,`celular`,`doc_pais`,`estado`) values (1,1,'Samuel bruno','961915596','51','1');

UNLOCK TABLES;

/*Table structure for table `mesnajes` */

DROP TABLE IF EXISTS `mesnajes`;

CREATE TABLE `mesnajes` (
  `mensaje_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `mensaje` longtext DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `tipo_msg` char(1) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`mensaje_id`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `mesnajes_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `mesnajes` */

LOCK TABLES `mesnajes` WRITE;

UNLOCK TABLES;

/*Table structure for table `session_ws` */

DROP TABLE IF EXISTS `session_ws`;

CREATE TABLE `session_ws` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_crecion` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `session_ws` longtext DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `session_ws_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `session_ws` */

LOCK TABLES `session_ws` WRITE;

UNLOCK TABLES;

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) DEFAULT NULL,
  `clave` varchar(200) DEFAULT NULL,
  `email` varchar(220) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp(),
  `token_pass` varchar(150) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `usuarios` */

LOCK TABLES `usuarios` WRITE;

insert  into `usuarios`(`usuario_id`,`usuario`,`clave`,`email`,`nombre`,`apellido`,`fecha_creacion`,`token_pass`,`estado`) values (1,'demo','f309e7b15a866ec3cf4d46b0911d01965b147530','demo@demo.com','demo','demo','2021-07-07 22:50:58','','1');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
